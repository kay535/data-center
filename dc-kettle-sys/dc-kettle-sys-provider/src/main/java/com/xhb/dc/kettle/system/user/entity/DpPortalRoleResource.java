package com.xhb.dc.kettle.system.user.entity;

import lombok.Data;

/**
 * DpPortalRoleResource.
 */
@Data
public class DpPortalRoleResource {
    /**
     * 角色编号(PK).
     */
    private String roleId;

    /**
     * 资源编号(PK).
     */
    private String resourceId;

}
