package com.xhb.dc.kettle.system.summary.model;

import lombok.Data;

/**
 * ProjectOnline.
 */
@Data
public class ProjectOnline {

    private String status;

    private int cnt;

}
