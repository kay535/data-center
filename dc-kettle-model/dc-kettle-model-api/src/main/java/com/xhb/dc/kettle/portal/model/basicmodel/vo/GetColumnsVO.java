package com.xhb.dc.kettle.portal.model.basicmodel.vo;

import lombok.Data;

/**
 * GetColumnsVO.
 */
@Data
public class GetColumnsVO {

    private String dataSourceName;

    private String sql;
}
